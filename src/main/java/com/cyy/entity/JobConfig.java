package com.cyy.entity;

import java.io.Serializable;

import com.alibaba.fastjson.JSONObject;

public class JobConfig implements Serializable{

	private static final long serialVersionUID = -632166456084956589L;

	private String jobName;
	
	private String jobClass;
	
	private String jobType;
	
	private String cron;
	
	private Integer shardingTotalCount;
	
	private String shardingItemParameters;
	
	private String jobParameter;
	
	private Boolean failover;
	
	private Boolean misfire;
	
	private String description;
	
	private JSONObject jobProperties;
	
	private Boolean streamingProcess;
	
	private Boolean monitorExecution;
	
	private Integer maxTimeDiffSeconds;
	
	private Integer monitorPort;
	
	private Integer jobShardingStrategyClass;
	
	private Integer reconcileIntervalMinutes;
	
	private Boolean disabled;
	
	private Boolean overwrite;

	private String jobListenerClass;

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getJobClass() {
		return jobClass;
	}

	public void setJobClass(String jobClass) {
		this.jobClass = jobClass;
	}

	public String getJobType() {
		return jobType;
	}

	public void setJobType(String jobType) {
		this.jobType = jobType;
	}

	public String getCron() {
		return cron;
	}

	public void setCron(String cron) {
		this.cron = cron;
	}

	public Integer getShardingTotalCount() {
		return shardingTotalCount;
	}

	public void setShardingTotalCount(Integer shardingTotalCount) {
		this.shardingTotalCount = shardingTotalCount;
	}

	public String getShardingItemParameters() {
		return shardingItemParameters;
	}

	public void setShardingItemParameters(String shardingItemParameters) {
		this.shardingItemParameters = shardingItemParameters;
	}

	public String getJobParameter() {
		return jobParameter;
	}

	public void setJobParameter(String jobParameter) {
		this.jobParameter = jobParameter;
	}

	public Boolean getFailover() {
		return failover;
	}

	public void setFailover(Boolean failover) {
		this.failover = failover;
	}

	public Boolean getMisfire() {
		return misfire;
	}

	public void setMisfire(Boolean misfire) {
		this.misfire = misfire;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public JSONObject getJobProperties() {
		return jobProperties;
	}

	public void setJobProperties(JSONObject jobProperties) {
		this.jobProperties = jobProperties;
	}

	public Integer getMaxTimeDiffSeconds() {
		return maxTimeDiffSeconds;
	}

	public void setMaxTimeDiffSeconds(Integer maxTimeDiffSeconds) {
		this.maxTimeDiffSeconds = maxTimeDiffSeconds;
	}

	public Integer getMonitorPort() {
		return monitorPort;
	}

	public void setMonitorPort(Integer monitorPort) {
		this.monitorPort = monitorPort;
	}

	public Integer getJobShardingStrategyClass() {
		return jobShardingStrategyClass;
	}

	public void setJobShardingStrategyClass(Integer jobShardingStrategyClass) {
		this.jobShardingStrategyClass = jobShardingStrategyClass;
	}

	public Integer getReconcileIntervalMinutes() {
		return reconcileIntervalMinutes;
	}

	public void setReconcileIntervalMinutes(Integer reconcileIntervalMinutes) {
		this.reconcileIntervalMinutes = reconcileIntervalMinutes;
	}

	public Boolean getDisabled() {
		return disabled;
	}

	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}

	public Boolean getOverwrite() {
		return overwrite;
	}

	public void setOverwrite(Boolean overwrite) {
		this.overwrite = overwrite;
	}

	public Boolean getStreamingProcess() {
		return streamingProcess;
	}

	public void setStreamingProcess(Boolean streamingProcess) {
		this.streamingProcess = streamingProcess;
	}

	public Boolean getMonitorExecution() {
		return monitorExecution;
	}

	public void setMonitorExecution(Boolean monitorExecution) {
		this.monitorExecution = monitorExecution;
	}

	public String getJobListenerClass() {
		return jobListenerClass;
	}

	public void setJobListenerClass(String jobListenerClass) {
		this.jobListenerClass = jobListenerClass;
	}
}
