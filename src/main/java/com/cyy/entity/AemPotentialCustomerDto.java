package com.cyy.entity;


import java.io.Serializable;
import java.util.Date;

public class AemPotentialCustomerDto implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;

    private Integer isDeleted;

    private Long cid;

    private Date createTime;

    private Date updateTime;

    private Date deleteTime;

    private Long createUserId;

    private Long deleteUserId;

    private String name;

    private String nickName;

    private Long visitNum;

    private Long shareNum;

    private Integer customerType;

    private Date joinTime;

    private String tag;

    private String belongStaffIds;

    private Long customerId;
    
    private Long fansId;
    
    private String openId;
    
    private Integer visitNumStart;
    
    private Integer visitNumEnd;
    
    private Integer shareNumStart;
    
    private Integer shareNumEnd;
    
    private String joinTimeStart;
    
    private String joinTimeNumEnd;
    
    private String startTime;
    
    private String endTime;

    private String headImg;

    private String customerPhone;

    public String getHeadImg() {
        return headImg;
    }

    public void setHeadImg(String headImg) {
        this.headImg = headImg;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Long getCid() {
        return cid;
    }

    public void setCid(Long cid) {
        this.cid = cid;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getDeleteTime() {
        return deleteTime;
    }

    public void setDeleteTime(Date deleteTime) {
        this.deleteTime = deleteTime;
    }

    public Long getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
    }

    public Long getDeleteUserId() {
        return deleteUserId;
    }

    public void setDeleteUserId(Long deleteUserId) {
        this.deleteUserId = deleteUserId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName == null ? null : nickName.trim();
    }

    public Long getVisitNum() {
        return visitNum;
    }

    public void setVisitNum(Long visitNum) {
        this.visitNum = visitNum;
    }

    public Long getShareNum() {
        return shareNum;
    }

    public void setShareNum(Long shareNum) {
        this.shareNum = shareNum;
    }

    public Integer getCustomerType() {
        return customerType;
    }

    public void setCustomerType(Integer customerType) {
        this.customerType = customerType;
    }

    public Date getJoinTime() {
        return joinTime;
    }

    public void setJoinTime(Date joinTime) {
        this.joinTime = joinTime;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag == null ? null : tag.trim();
    }

    public String getBelongStaffIds() {
        return belongStaffIds;
    }

    public void setBelongStaffIds(String belongStaffIds) {
        this.belongStaffIds = belongStaffIds == null ? null : belongStaffIds.trim();
    }

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Long getFansId() {
		return fansId;
	}

	public void setFansId(Long fansId) {
		this.fansId = fansId;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public Integer getVisitNumStart() {
		return visitNumStart;
	}

	public void setVisitNumStart(Integer visitNumStart) {
		this.visitNumStart = visitNumStart;
	}

	public Integer getVisitNumEnd() {
		return visitNumEnd;
	}

	public void setVisitNumEnd(Integer visitNumEnd) {
		this.visitNumEnd = visitNumEnd;
	}

	public Integer getShareNumStart() {
		return shareNumStart;
	}

	public void setShareNumStart(Integer shareNumStart) {
		this.shareNumStart = shareNumStart;
	}

	public Integer getShareNumEnd() {
		return shareNumEnd;
	}

	public void setShareNumEnd(Integer shareNumEnd) {
		this.shareNumEnd = shareNumEnd;
	}

	public String getJoinTimeStart() {
		return joinTimeStart;
	}

	public void setJoinTimeStart(String joinTimeStart) {
		this.joinTimeStart = joinTimeStart;
	}

	public String getJoinTimeNumEnd() {
		return joinTimeNumEnd;
	}

	public void setJoinTimeNumEnd(String joinTimeNumEnd) {
		this.joinTimeNumEnd = joinTimeNumEnd;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }
}