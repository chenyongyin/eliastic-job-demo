package com.cyy.execute;

import com.alibaba.fastjson.JSON;
import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.simple.SimpleJob;

/**
 * @author wschenyongyin
 * @version 1.0
 * @Title: JavaSimpleJob
 * @ProjectName eliastic-job-demo
 * @Description: TODO
 * @date 2019/3/1815:22
 */
public class JavaSimpleJob implements SimpleJob {
    @Override
    public void execute(ShardingContext shardingContext) {
        System.out.println("enter into JavaSimpleJob--->"+JSON.toJSONString(shardingContext));
    }
}
