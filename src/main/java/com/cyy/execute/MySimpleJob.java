package com.cyy.execute;

import com.alibaba.fastjson.JSON;
import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.simple.SimpleJob;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author wschenyongyin
 * @version 1.0
 * @Title: MySimpleJob
 * @ProjectName eliastic-job-demo
 * @Description: 简单job
 * @date 2019/3/1810:57
 */
public class MySimpleJob implements SimpleJob {
    /** 模拟数据*/
    private static  Map<String,List<Map<String,String>>> customerListData = new HashMap<>(5);

    static {
        List<Map<String, String>> beiJingCustomerList = new ArrayList<>();
        List<Map<String, String>> shangHaiCustomerList = new ArrayList<>();
        List<Map<String, String>> nanJingCustomerList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Map<String, String> beiJingCustomer = new HashMap<String, String>(4);
            beiJingCustomer.put("name", "北京会员" + i);
            beiJingCustomer.put("age", i + "");
            Map<String, String> shangHaiCustomer = new HashMap<String, String>(4);
            shangHaiCustomer.put("name", "上海会员" + i);
            shangHaiCustomer.put("age", i + "");
            Map<String, String> nanJingCustomer = new HashMap<String, String>(4);
            nanJingCustomer.put("name", "南京会员" + i);
            nanJingCustomer.put("age", i + "");
            beiJingCustomerList.add(beiJingCustomer);
            shangHaiCustomerList.add(shangHaiCustomer);
            nanJingCustomerList.add(nanJingCustomer);
        }
        customerListData.put("北京", beiJingCustomerList);
        customerListData.put("上海", shangHaiCustomerList);
        customerListData.put("南京", nanJingCustomerList);
    }

    @Override
    public void execute(ShardingContext shardingContext) {
        String shardingParameter = shardingContext.getShardingParameter();
        /** 本job设置的分片参数为"0=北京,1=上海,2=南京",将任务分成三片 并且集群部署数量也是3 那么每台服务器的execute会执行，并且每台服务器接收到的ShardingItem分别为0、1、2，shardingParameter分别为北京、上海、南京
         * 开发人员将根据自身业务进行处理
         * */

        switch (shardingContext.getShardingItem()){
            // 对城市为北京的会员数据进行处理
            case 0:
                System.out.println("北京的会员数据:"+JSON.toJSONString(queryCustomerLisyByCity(shardingContext.getShardingParameter())));
                break;
            // 对城市为上海的会员数据进行处理
            case 1:
                System.out.println("上海的会员数据:"+JSON.toJSONString(queryCustomerLisyByCity(shardingContext.getShardingParameter())));
                break;
            // 对城市为南京的会员数据进行处理
            case 2:
                System.out.println("南京的会员数据:"+JSON.toJSONString(queryCustomerLisyByCity(shardingContext.getShardingParameter())));
                break;
                default:
                    break;
        }
    }

    /**
    　　* @Description: 数据查询
    　　* @param [city]
    　　* @return java.util.List<java.util.Map<java.lang.String,java.lang.String>>
    　　* @throws
    　　* @author wschenyongyin
    　　* @date 2019/3/18 14:16
    　　*/
    private List<Map<String,String>> queryCustomerLisyByCity(String city){
         return customerListData.get(city);
    }


}
