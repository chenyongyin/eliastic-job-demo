package com.cyy.execute;

import com.alibaba.fastjson.JSON;
import com.cyy.entity.AemPotentialCustomerDto;
import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.dataflow.DataflowJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.applet.Main;

import java.util.ArrayList;
import java.util.List;

/**
 * @author wschenyongyin
 * @version 1.0
 * @Title: MyDataflowJob
 * @ProjectName eliastic-job-demo
 * @Description: DataflowJob  1.fetchData会等processData执行完成后再执行  2.
 *
 * 流式处理数据只有fetchData方法的返回值为null或集合长度为空时，作业才停止抓取，否则作业将一直运行下去；（streaming-process="true" ）
 * 非流式处理数据则只会在每次作业执行过程中执行一次fetchData方法和processData方法，随即完成本次作业。（streaming-process="false" ）
 *
 * @date 2019/3/1810:58
 */
public class MyDataflowJob implements DataflowJob<AemPotentialCustomerDto> {
    private static Logger logger = LoggerFactory.getLogger(MyDataflowJob.class);
    private Long EXECUTE_INDEX = 0L;
    private Long EXECUTE_MAX = 10L;
    @Override
    public List<AemPotentialCustomerDto> fetchData(ShardingContext shardingContext) {
        EXECUTE_INDEX++;
        System.out.println("enter into fetchData--->"+JSON.toJSONString(shardingContext));
        AemPotentialCustomerDto aemPotentialCustomerDto = new AemPotentialCustomerDto();
        aemPotentialCustomerDto.setCid(17L);
        aemPotentialCustomerDto.setId(EXECUTE_INDEX);
        List<AemPotentialCustomerDto> aemPotentialCustomerDtoList = new ArrayList<>();
        aemPotentialCustomerDtoList.add(aemPotentialCustomerDto);
        if (EXECUTE_INDEX < EXECUTE_MAX){
            return aemPotentialCustomerDtoList;
        }else{
            // 处理数据完成
            return null;
        }

    }

    @Override
    public void processData(ShardingContext shardingContext, List<AemPotentialCustomerDto> data) {
        try {
            Thread.currentThread().sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("enter into processData--->"+JSON.toJSONString(shardingContext));
        System.out.println("data--->"+JSON.toJSONString(data));
    }
}
