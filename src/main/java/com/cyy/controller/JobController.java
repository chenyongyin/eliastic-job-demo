package com.cyy.controller;

import com.cyy.entity.JobConfig;
import com.cyy.execute.JavaSimpleJob;
import com.cyy.listener.MyJobListener;
import com.cyy.service.IJobOperateService;
//import com.dangdang.ddframe.job.context.CreateModeEnum;
import com.dangdang.ddframe.job.lite.lifecycle.domain.JobSettings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author wschenyongyin
 * @version 1.0
 * @Title: JobController
 * @ProjectName eliastic-job-demo
 * @Description: TODO
 * @date 2019/3/1815:23
 */
@Controller
@RequestMapping("/job")
public class JobController {

    public JobController(){
        System.out.println("init---------->");
    }

    @Autowired(required = false)
    private IJobOperateService jobOperateService;

    @RequestMapping(value = "/createJob")
    @ResponseBody
    public void createJob(String jobName) throws Exception{
        JobSettings JobConfig = new JobSettings();
        JobConfig.setJobName(jobName);
        JobConfig.setCron("*/60 * * * * ?");
        JobConfig.setJobParameter("my name is simpleJob");
        JobConfig.setShardingTotalCount(3);
        JobConfig.setShardingItemParameters("0=北京,1=上海,2=南京");
        JobConfig.setJobClass(JavaSimpleJob.class.getCanonicalName());
//        JobConfig.setJobListenerClass(MyJobListener.class.getCanonicalName());
        JobConfig.setFailover(true);
        JobConfig.setMisfire(true);
        JobConfig.setJobType("SIMPLE");
//        JobConfig.setCreateMode(CreateModeEnum.API.name());
        jobOperateService.createJob(JobConfig);
    }
    @RequestMapping(value = "/deleteJob")
    @ResponseBody
    public void deleteJob(String jobName){
        JobConfig JobConfig = new JobConfig();
        JobConfig.setJobName(jobName);

        jobOperateService.deleteJob(JobConfig);
    }
    @RequestMapping(value = "/disableJob")
    @ResponseBody
    public void disableJob(String jobName){
        JobConfig JobConfig = new JobConfig();
        JobConfig.setJobName(jobName);
        JobConfig.setFailover(true);
        jobOperateService.disableJob(JobConfig);
    }
    @RequestMapping(value = "/enableJob")
    @ResponseBody
    public void enableJob(String jobName){
        JobConfig JobConfig = new JobConfig();
        JobConfig.setJobName(jobName);
        JobConfig.setFailover(true);
        jobOperateService.enableJob(JobConfig);
    }


    @RequestMapping(value = "/shutdownJob")
    @ResponseBody
    public void shutdownJob(String jobName){
        JobConfig JobConfig = new JobConfig();
        JobConfig.setJobName(jobName);
        JobConfig.setFailover(true);
        jobOperateService.shutdownJob(JobConfig);
    }

    @RequestMapping(value = "/triggerJob")
    @ResponseBody
    public void tiggerJob(String jobName){
        JobConfig JobConfig = new JobConfig();
        JobConfig.setJobName(jobName);
        JobConfig.setFailover(true);
        jobOperateService.triggerJob(JobConfig);
    }

    @RequestMapping(value = "/addTriggerJobListenter")
    @ResponseBody
    public void addTriggerJobListenter(String jobName){

        jobOperateService.addTriggerJobListenter(jobName);
    }

}
