package com.cyy.service;


import com.cyy.entity.JobConfig;
import com.dangdang.ddframe.job.lite.lifecycle.domain.JobSettings;

/**
 * @author wschenyongyin
 * @version 1.0
 * @Title: IPotentialCustomerService
 * @ProjectName eliastic-job-demo
 * @Description: TODO
 * @date 2019/3/1813:15
 */
public interface IJobOperateService {

    Boolean createJob(JobSettings JobConfig) throws Exception;

    Boolean deleteJob(JobConfig jobConfig);

    Boolean disableJob(JobConfig jobConfig);

    Boolean enableJob(JobConfig jobConfig);

    Boolean shutdownJob(JobConfig jobConfig);

    Boolean triggerJob(JobConfig jobConfig);

    Boolean reloadAllJob();

    void addTriggerJobListenter(String jobName);
}
