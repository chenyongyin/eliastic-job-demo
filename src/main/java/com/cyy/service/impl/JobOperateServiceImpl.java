package com.cyy.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.cyy.entity.JobConfig;
import com.cyy.listener.TriggerJobListenter;
import com.cyy.service.IJobOperateService;
import com.dangdang.ddframe.job.api.JobType;
import com.dangdang.ddframe.job.config.JobCoreConfiguration;
import com.dangdang.ddframe.job.config.JobTypeConfiguration;
import com.dangdang.ddframe.job.config.dataflow.DataflowJobConfiguration;
import com.dangdang.ddframe.job.config.simple.SimpleJobConfiguration;
import com.dangdang.ddframe.job.lite.api.JobScheduler;
import com.dangdang.ddframe.job.lite.api.listener.ElasticJobListener;
import com.dangdang.ddframe.job.lite.config.LiteJobConfiguration;
import com.dangdang.ddframe.job.lite.internal.storage.JobNodePath;
import com.dangdang.ddframe.job.lite.lifecycle.api.JobOperateAPI;
import com.dangdang.ddframe.job.lite.lifecycle.domain.JobSettings;
import com.dangdang.ddframe.job.lite.lifecycle.internal.operate.JobOperateAPIImpl;
import com.dangdang.ddframe.job.reg.base.CoordinatorRegistryCenter;
import com.dangdang.ddframe.job.reg.zookeeper.ZookeeperConfiguration;
import com.dangdang.ddframe.job.reg.zookeeper.ZookeeperRegistryCenter;
import com.google.common.base.Optional;
import jodd.util.StringUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.curator.framework.recipes.cache.TreeCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author wschenyongyin
 * @version 1.0
 * @Title: PotentialCustomerServiceImpl
 * @ProjectName eliastic-job-demo
 * @Description: TODO
 * @date 2019/3/1813:16
 */
@Service
public class JobOperateServiceImpl implements IJobOperateService {
    @Autowired
    private ZookeeperRegistryCenter registryCenter;

    public  JobOperateServiceImpl(ZookeeperRegistryCenter registryCenter){
        this.registryCenter = registryCenter;
    }

    @Override
    public Boolean createJob(JobSettings JobConfig) throws Exception {
        JobOperateAPI jobOperateAPI = new JobOperateAPIImpl(registryCenter);
//        jobOperateAPI.createJob(JobConfig);
        return true;
    }

    @Override
    public Boolean deleteJob(JobConfig jobConfig) {
        JobOperateAPI jobOperateAPI = new JobOperateAPIImpl(registryCenter);
        Optional<String> jobNameOpt = Optional.of(jobConfig.getJobName());
        Optional<String> serverIp =  Optional.fromNullable(null);
        jobOperateAPI.remove(jobNameOpt,serverIp);
        return null;
    }

    @Override
    public Boolean disableJob(JobConfig jobConfig) {
        JobOperateAPI jobOperateAPI = new JobOperateAPIImpl(registryCenter);
        Optional<String> jobNameOpt = Optional.of(jobConfig.getJobName());
        Optional<String> serverIp =  Optional.fromNullable(null);
        jobOperateAPI.disable(jobNameOpt,serverIp);
        return true;
    }

    @Override
    public Boolean enableJob(JobConfig jobConfig) {
        JobOperateAPI jobOperateAPI = new JobOperateAPIImpl(registryCenter);
        Optional<String> jobNameOpt = Optional.of(jobConfig.getJobName());
        Optional<String> serverIp =  Optional.fromNullable(null);
        jobOperateAPI.enable(jobNameOpt,serverIp);
        return true;
    }

    /**
    　　* @Description: shutdown的时候往zk的job节点上增加shutdown节点 便于重启任务的时候过滤shutdown任务
    　　* @param [jobBean]
    　　* @return java.lang.Boolean
    　　* @throws
    　　* @author wschenyongyin
    　　* @date 2019/3/19 15:36
    　　*/
    @Override
    public Boolean shutdownJob(JobConfig jobConfig) {
        JobOperateAPI jobOperateAPI = new JobOperateAPIImpl(registryCenter);
        Optional<String> jobNameOpt = Optional.of(jobConfig.getJobName());
        Optional<String> serverIp =  Optional.fromNullable(null);
        jobOperateAPI.shutdown(jobNameOpt,serverIp);
        JobNodePath jobNodePath = new JobNodePath(jobConfig.getJobName());
//        registryCenter.persist(jobNodePath.getShutdownNodePath(), "true");
        CoordinatorRegistryCenter regCenter = new ZookeeperRegistryCenter(new ZookeeperConfiguration("zk_host:2181", "elastic-job-demo"));
        regCenter.init();
        return true;
    }

    @Override
    public Boolean triggerJob(JobConfig jobConfig) {
        JobOperateAPI jobOperateAPI = new JobOperateAPIImpl(registryCenter);
        Optional<String> jobNameOpt = Optional.of(jobConfig.getJobName());
        Optional<String> serverIp =  Optional.fromNullable(null);
        jobOperateAPI.trigger(jobNameOpt,serverIp);
        return true;
    }

    /**
    　　* @Description: 重新加载未删除和未shutdown的任务
    　　* @param []
    　　* @return java.lang.Boolean
    　　* @throws
    　　* @author wschenyongyin
    　　* @date 2019/3/19 17:43
    　　*/
    @Override
    public Boolean reloadAllJob() {

        return true;
    }

    @Override
    public void addTriggerJobListenter(String jobName) {
        TriggerJobListenter triggerJobListenter = new TriggerJobListenter();
        JobNodePath jobNodePath = new JobNodePath(jobName);
        System.out.println(jobNodePath.getInstancesNodePath());
        TreeCache cache11 = new TreeCache(registryCenter.getClient(),"/"+jobName);
        try {
            cache11.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
        TreeCache cache = (TreeCache) registryCenter.getRawCache("/"+jobName);
        cache11.getListenable().addListener(triggerJobListenter);
    }


}
