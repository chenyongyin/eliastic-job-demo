package com.cyy.listener;

import com.google.common.base.Charsets;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.recipes.cache.ChildData;
import org.apache.curator.framework.recipes.cache.TreeCacheEvent;
import org.apache.curator.framework.recipes.cache.TreeCacheListener;

/**
 * @author wschenyongyin
 * @version 1.0
 * @Title: AbstractJobListenter
 * @ProjectName eliastic-job-demo
 * @Description: TODO
 * @date 2019/8/6 14:51
 */
public abstract class AbstractJobListenter implements TreeCacheListener {
    @Override
    public void childEvent(CuratorFramework curatorFramework, TreeCacheEvent treeCacheEvent) throws Exception {
        ChildData childData = treeCacheEvent.getData();
        if (null == childData) {
            return;
        }
        String path = childData.getPath();
        if (path.isEmpty()) {
            return;
        }
        dataChangeNotify(path, treeCacheEvent.getType(), null == childData.getData() ? "" : new String(childData.getData(), Charsets.UTF_8));
    }
    /**
     * @author chenyongyin
     * @description 数据变换监听
     * @date 2019/8/6 14:55
     * @params [path, eventType, data]
     * @return void
     **/
    abstract void dataChangeNotify(String path, final TreeCacheEvent.Type eventType, final String data );
}
