package com.cyy.listener;

import com.alibaba.fastjson.JSON;
import com.dangdang.ddframe.job.executor.ShardingContexts;
import com.dangdang.ddframe.job.lite.api.listener.ElasticJobListener;

/**
 * @author wschenyongyin
 * @version 1.0
 * @Title: MyJobListener
 * @ProjectName eliastic-job-demo
 * @Description: TODO
 * @date 2019/3/1814:27
 */
public class MyJobListener implements ElasticJobListener {
    @Override
    public void beforeJobExecuted(ShardingContexts shardingContexts) {
        System.out.println("enter into jobListenter beforeJobExecuted"+JSON.toJSONString(shardingContexts));
    }

    @Override
    public void afterJobExecuted(ShardingContexts shardingContexts) {
        System.out.println("enter into jobListenter afterJobExecuted"+JSON.toJSONString(shardingContexts));
    }
}
