package com.cyy.listener;

import org.apache.curator.framework.recipes.cache.TreeCacheEvent;

/**
 * @author wschenyongyin
 * @version 1.0
 * @Title: TriggerJobListenter
 * @ProjectName eliastic-job-demo
 * @Description: 监听触发任务节点变化
 * @date 2019/8/6 14:50
 */
public class TriggerJobListenter extends AbstractJobListenter {

    @Override
    void dataChangeNotify(String path, TreeCacheEvent.Type eventType, String data) {
        System.out.println("path----->"+path);
        System.out.println("eventType----->"+eventType.name());
        System.out.println("data----->"+data);
    }
}
