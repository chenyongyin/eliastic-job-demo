package com.cyy.test;

/**
 * @author wschenyongyin
 * @version 1.0
 * @Title: AbstractJobExcute
 * @ProjectName eliastic-job-demo
 * @Description: TODO
 * @date 2019/7/4 17:35
 */
public abstract class AbstractJobExcute implements JobExecuteInterface{
    public void start(){
        System.out.println("start");
        handle();
        execute();
    }

    abstract void handle();
}
