package com.cyy.test;

/**
 * @author wschenyongyin
 * @version 1.0
 * @Title: AbstractJobExcuteInterface
 * @ProjectName eliastic-job-demo
 * @Description: TODO
 * @date 2019/7/4 17:36
 */
public interface JobExecuteInterface {

    void execute();

}
