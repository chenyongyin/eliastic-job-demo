package com.cyy.test;

/**
 * @author wschenyongyin
 * @version 1.0
 * @Title: SimpleJobExecute
 * @ProjectName eliastic-job-demo
 * @Description: TODO
 * @date 2019/7/4 17:41
 */
public class SimpleJobExecute extends AbstractJobExcute{
    @Override
    void handle() {
        System.out.println("handle---------->");
    }

    @Override
    public void execute() {
        System.out.println("execute---------->");
    }


    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        AbstractJobExcute abstractJobExcute = (AbstractJobExcute) Class.forName("com.cyy.test.SimpleJobExecute").newInstance();
        abstractJobExcute.start();
    }
}
