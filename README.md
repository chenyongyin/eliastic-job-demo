## 本项目包含内容如下：
> 1.eliastic-job spring配置示例

> 2.eliastic-job java创建job示例

> 3.介绍了simplejob和dataflow、以及分片的用法

> 4.介绍了任务停用、启用、删除等操作

> 5.实现重启系统后自动加载未被删除和shutdown的任务（eliastic-job本身未实现）

> 6.升级重启时对任务状态(enable、disable)的恢复